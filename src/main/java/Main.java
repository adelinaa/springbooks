import beans.Author;
import beans.Book;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

        Book book1 = (Book)applicationContext.getBean("book");
        book1.setName("Mrs. Dalloway");

        Author author = (Author)applicationContext.getBean("author");
        author.setName("Virginia Woolf");
        author.setAge(50);
        book1.setAuthor(author);
        System.out.println(book1.getBookInfo());



    }
}
