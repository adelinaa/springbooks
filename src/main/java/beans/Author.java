package beans;

public class Author {
    private String name;
    private int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAuthorInfo(){
        return "author name: " + name + "author age: " + age;
    }
}
