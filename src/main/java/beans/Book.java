package beans;

public class Book {
    private Author author;
    private String name;

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBookInfo(){
        return "Book: " + name + author.getAuthorInfo() ;
    }
}
